﻿using System;
using GameManager;

namespace Monopoly
{
    class Program
    {

        static void Main(string[] args)
        {  
            var utils = new Utils();
            utils.checkGameFiles();

            Console.WriteLine("Welcome to Monopoly USA.");
            Console.WriteLine("\nSelect an option:");
            Console.WriteLine("[n]ew game");
            Console.WriteLine("[c]ontinue a game");
            Console.WriteLine("[q]uit game");

            while (true)
            {
                Console.Write("\n$: ");
                char selectedOption;
                try {
                    selectedOption = Console.ReadLine()[0];
                } catch (System.IndexOutOfRangeException) 
                {
                    continue;
                }

                if (selectedOption == 'n')
                {
                    Game gameInst = utils.setupGameInstance();
                    utils.generateBoard(gameInst);
                    utils.startGame(gameInst);
                }
                else if (selectedOption == 'c')
                {
                    Console.WriteLine("Hehe");
                } else if (selectedOption == 'q')
                { 
                    Environment.Exit(0);
                } else {
                    Console.WriteLine("Command not found :(");
                }
            }
        }
    }
}

