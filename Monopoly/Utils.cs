﻿using System.Net;
using Newtonsoft.Json;

namespace GameManager
{
    public class Utils
    {
        public string rootPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".monopoly");
        public string propFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".monopoly", "props.json");

        public async Task checkGameFiles()
        {
            Console.WriteLine("Checking game files....");
            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }

            if (File.Exists(propFilePath))
            {
                File.Delete(propFilePath);
            }

            Console.WriteLine("Downloading game files....");
            WebClient web = new WebClient();
            web.DownloadFile(new Uri("https://gitlab.com/elwinjyot/monopoly/-/raw/game-data/Monopoly/props.json?inline=false"), propFilePath);
            Console.WriteLine("Download finished");
            Console.WriteLine("Checkup finished...no issues found :), Starting game.");
            System.Threading.Thread.Sleep(1000);
            Console.Clear();
        }
        public void generateBoard(Game game)
        {
            using (StreamReader file = new StreamReader(propFilePath))
            {
                string json = file.ReadToEnd();
                game.board = JsonConvert.DeserializeObject<List<Property>>(json);
            }
        }
        public Game setupGameInstance()
        {
            Console.Write("\nHow many of you are playing?: ");
            int playerInstanceCount = Int32.Parse(Console.ReadLine());

            // Setup Instances
            Game game = new Game();
            game.currPlayerId = 0;
            for (int i = 0; i < playerInstanceCount; i++)
            {
                Console.Write($"Player {i+ 1}'s name: ");
                string playerName = Console.ReadLine();
                Player player = new Player(i, playerName);
                game.players.Add(player);
            }

            game.currPlayer = game.players[game.currPlayerId];
            return game;
        }

        public void startGame(Game game)
        {
            game.running = true;

            Console.WriteLine("\nType 'h' for command list");
            while (game.running)
            { 
                Console.Write("\n{0} > ", game.players[game.currPlayerId].getPlayerName());
                char? cmd;
                try
                {
                    cmd = Console.ReadLine()[0];
                } catch (System.IndexOutOfRangeException) {
                    continue;
                }

                if (cmd == 'r')
                {
                    int rolled = rollDice(game);
                    Console.WriteLine("You rolled: {0}", rolled);
                    movePawn(player: game.currPlayer, rolled: rolled);
                    Property prop = showPropertyCard(game);
                    buyingOption(game, prop);
                } else if (cmd == 'q') 
                {
                    Environment.Exit(0);
                } else if (cmd == 'h')
                {
                    Console.WriteLine("[r]oll dice");
                    Console.WriteLine("[q]uit game");
                    Console.WriteLine("[s]ave game");
                    Console.WriteLine("[i]nfo of player");
                    Console.WriteLine("[h]elp");
                } else if (cmd == 'i')
                {
                    showPlayerInfo(player: game.currPlayer);
                }
            }
        }

        private static int rollDice(Game game)
        {
            var rand = new Random();
            return rand.Next(1, 7);
        }

        private static void movePawn(Player player, int rolled)
        {
            player.setPlayerPos(0);
            // player.playerPos += rolled;
        }

        private static Property showPropertyCard(Game game) 
        {
            Property landedProperty = game.board[game.currPlayer.getPlayerPos()];
            Console.WriteLine($"\n{landedProperty.propName}\n");
            Console.WriteLine($"Cost of Land: {landedProperty.costOfLand}");
            Console.WriteLine($"\nRent with Land: {landedProperty.rent["land"]}");
            Console.WriteLine($"Rent with 1 house: {landedProperty.rent["1house"]}");
            Console.WriteLine($"Rent with 2 house: {landedProperty.rent["2house"]}");
            Console.WriteLine($"Rent with 3 house: {landedProperty.rent["3house"]}");
            Console.WriteLine($"Rent with hotel: {landedProperty.rent["hotel"]}");

            return landedProperty;
        }

        private static void buyingOption(Game game, Property prop)
        {
            if (!prop.sold)
            {
                while (true)
                {
                    Console.Write("Do you want to buy this property? [y/n]: ");
                    char confirmation = Console.ReadLine()[0];

                    if (confirmation == 'y')
                    {
                        buyProperty(game, game.currPlayer);
                        break;
                    }
                    else if (confirmation == 'n')
                    {
                        break;
                    } else
                    {
                        Console.WriteLine("Please enter 'y' or 'n'");
                        continue;
                    }
                }
            } else
            {
                Console.WriteLine("Rent payed");
            }

            if (game.currPlayerId == game.players.Count - 1)
            {
                game.currPlayerId = 0;
            }
            else
            {
                game.currPlayerId += 1;
            }
            game.currPlayer = game.players[game.currPlayerId];
        }

        private static void buyProperty(Game game, Player player)
        {
            game.board[game.currPlayer.getPlayerPos()].sold = true;
            game.board[player.getPlayerPos()].soldTo = player.getPlayerId();
            game.currPlayer.addProperty(game.board[player.getPlayerPos()]);
            player.debitAmount(game.board[game.currPlayer.getPlayerPos()].costOfLand);
        }

        private static void showPlayerInfo(Player player)
        {
            Console.WriteLine($"\n0{player.getPlayerId() + 1}|{player.getPlayerName().ToUpper()}");
            Console.WriteLine($"Account balance: ${player.getBalance()}");
            Console.WriteLine($"{player.getOwnedProperties().Count} properties owned");
            if (player.getOwnedProperties().Count != 0)
            {
                Console.WriteLine("\nOwned Properties-------------");
                foreach (Property prop in player.getOwnedProperties())
                {
                    Console.WriteLine(prop.propName);
                }
            }
        }
    }

    public class Player
    {
        private int id;
        private string? playerName;
        private int playerPos = 0;
        private double balance = 0;
        private List<Property> ownedProperties = new List<Property> {  };

        public Player (int id, string? playerName)
        {
            this.id = id;
            this.playerName = playerName;
        }

        public void setPlayerId(int id)
        {
            this.id = id;
        }

        public void setPlayerName(string playerName)
        {
            this.playerName = playerName;
        }

        public void setPlayerPos(int rolled)
        {
            this.playerPos += playerPos;
        }

        public void creditAmount(double balance)
        {
            this.balance += balance; 
        }

        public void debitAmount(double balance)
        {
            this.balance -= balance;
        }

        public int getPlayerId()
        {
            return id;
        }

        public string getPlayerName()
        {
            return playerName;
        }

        public double getBalance()
        {
            return balance;
        }

        public int getPlayerPos ()
        {
            return playerPos;
        }

        public List<Property> getOwnedProperties()
        {
            return ownedProperties;
        }

        public void addProperty (Property prop)
        {
            ownedProperties.Add(prop);
        }
    }

    public class Property
    {
        public string? propName;
        public string? colorGroup;
        public double costOfLand = 0;
        public double costOfHouse = 0;
        public double costOfHotel = 0;
        public bool sold = false;
        public int? soldTo;

        public Dictionary<string, double> rent = new Dictionary<string, double> {
            { "land", 0.0 },
            { "1house", 0.0 },
            { "2house", 0.0 },
            { "3house", 0.0 },
            { "hotel" , 0.0 },
        };
    }


    public class Game
    {
        public bool running = false;
        public List<Player> players = new List<Player> { };
        public List<Property> board = new List<Property> { };
        public Player? currPlayer;
        public int currPlayerId;
    }
}
